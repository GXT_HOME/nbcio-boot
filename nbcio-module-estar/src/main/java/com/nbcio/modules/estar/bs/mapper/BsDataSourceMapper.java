package com.nbcio.modules.estar.bs.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.nbcio.modules.estar.bs.entity.BsDataSource;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: bs_data_source
 * @Author: nbacheng
 * @Date:   2023-03-14
 * @Version: V1.0
 */
public interface BsDataSourceMapper extends BaseMapper<BsDataSource> {

}
