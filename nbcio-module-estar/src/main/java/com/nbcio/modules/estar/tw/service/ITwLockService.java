package com.nbcio.modules.estar.tw.service;

import com.nbcio.modules.estar.tw.entity.TwLock;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 项目端节点表
 * @Author: nbacheng
 * @Date:   2023-07-03
 * @Version: V1.0
 */
public interface ITwLockService extends IService<TwLock> {

}
