package com.nbcio.modules.estar.oa.vo;

import lombok.Data;

@Data
public class ScheduleTime {
    private String startTime;
    private String endTime;
    private String ids;
}
