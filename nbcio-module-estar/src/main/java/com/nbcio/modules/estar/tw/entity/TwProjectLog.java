package com.nbcio.modules.estar.tw.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.nbcio.modules.flowable.apithird.entity.SysUser;

import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 项目日志表
 * @Author: nbacheng
 * @Date:   2023-07-03
 * @Version: V1.0
 */
@Data
@TableName("tw_project_log")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="tw_project_log对象", description="项目日志表")
public class TwProjectLog implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
	/**操作人id*/
	@Excel(name = "操作人id", width = 15)
    @ApiModelProperty(value = "操作人id")
    private java.lang.String memberId;
	/**操作内容*/
	@Excel(name = "操作内容", width = 15)
    @ApiModelProperty(value = "操作内容")
    private java.lang.String content;
	/**remark*/
	@Excel(name = "remark", width = 15)
    @ApiModelProperty(value = "remark")
    private java.lang.String remark;
	/**操作类型*/
	@Excel(name = "操作类型", width = 15)
    @ApiModelProperty(value = "操作类型")
    private java.lang.String opeType;
	/**添加时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "添加时间")
    private java.util.Date createTime;
	/**任务id*/
	@Excel(name = "任务id", width = 15)
    @ApiModelProperty(value = "任务id")
    private java.lang.String sourceId;
	/**场景类型*/
	@Excel(name = "场景类型", width = 15)
    @ApiModelProperty(value = "场景类型")
    private java.lang.String actionType;
	/**toMemberId*/
	@Excel(name = "toMemberId", width = 15)
    @ApiModelProperty(value = "toMemberId")
    private java.lang.String toMemberId;
	/**是否评论，0：否*/
	@Excel(name = "是否评论，0：否", width = 15)
    @ApiModelProperty(value = "是否评论，0：否")
    private java.lang.Integer isComment;
	/**projectId*/
	@Excel(name = "projectId", width = 15)
    @ApiModelProperty(value = "projectId")
    private java.lang.String projectId;
	/**icon*/
	@Excel(name = "icon", width = 15)
    @ApiModelProperty(value = "icon")
    private java.lang.String icon;
	/**是否机器人*/
	@Excel(name = "是否机器人", width = 15)
    @ApiModelProperty(value = "是否机器人")
    private java.lang.Integer isRobot;
	
	@TableField(exist = false)
    private SysUser member;
}
