package com.nbcio.modules.estar.nd.file;

import lombok.Data;

@Data
public class ReadFile {
    private String fileUrl;
}
