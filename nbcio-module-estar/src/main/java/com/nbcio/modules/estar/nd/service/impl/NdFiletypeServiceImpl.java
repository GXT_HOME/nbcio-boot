package com.nbcio.modules.estar.nd.service.impl;

import com.nbcio.modules.estar.nd.entity.NdFiletype;
import com.nbcio.modules.estar.nd.mapper.NdFiletypeMapper;
import com.nbcio.modules.estar.nd.service.INdFiletypeService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: nd_filetype
 * @Author: nbacheng
 * @Date:   2023-04-08
 * @Version: V1.0
 */
@Service
public class NdFiletypeServiceImpl extends ServiceImpl<NdFiletypeMapper, NdFiletype> implements INdFiletypeService {

}
