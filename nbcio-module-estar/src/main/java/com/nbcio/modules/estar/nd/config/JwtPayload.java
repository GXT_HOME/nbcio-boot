package com.nbcio.modules.estar.nd.config;

import lombok.Data;

@Data
public class JwtPayload {
    private RegisterdClaims registerdClaims;

}

