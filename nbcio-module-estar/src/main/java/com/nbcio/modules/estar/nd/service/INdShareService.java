package com.nbcio.modules.estar.nd.service;

import com.nbcio.modules.estar.nd.dto.ShareFileDTO;
import com.nbcio.modules.estar.nd.dto.ShareListDTO;
import com.nbcio.modules.estar.nd.entity.NdShare;
import com.nbcio.modules.estar.nd.vo.ShareFileVO;
import com.nbcio.modules.estar.nd.vo.ShareListVO;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 网盘分享表
 * @Author: nbacheng
 * @Date:   2023-04-06
 * @Version: V1.0
 */
public interface INdShareService extends IService<NdShare> {

	List<ShareListVO> selectShareList(ShareListDTO shareListDTO, String username);

	ShareFileVO shareFile(ShareFileDTO shareSecretDTO);

}
