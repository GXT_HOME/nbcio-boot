package com.nbcio.modules.estar.nd.config;

import lombok.Data;

@Data
public class JwtHeader {
    private String alg;
    private String typ;
}
