package com.nbcio.modules.estar.tw.mapper;

import com.nbcio.modules.estar.tw.entity.TwOrganization;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 项目组织表
 * @Author: nbacheng
 * @Date:   2023-05-27
 * @Version: V1.0
 */
public interface TwOrganizationMapper extends BaseMapper<TwOrganization> {

}
