package com.nbcio.modules.estar.nd.service;

import com.nbcio.modules.estar.nd.entity.NdImage;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 网盘图像表
 * @Author: nbacheng
 * @Date:   2023-04-06
 * @Version: V1.0
 */
public interface INdImageService extends IService<NdImage> {

}
