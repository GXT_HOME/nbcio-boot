package com.nbcio.modules.estar.tw.mapper;

import com.nbcio.modules.estar.tw.entity.TwProjectTemplate;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 项目模板表
 * @Author: nbacheng
 * @Date:   2023-05-27
 * @Version: V1.0
 */
public interface TwProjectTemplateMapper extends BaseMapper<TwProjectTemplate> {

}
