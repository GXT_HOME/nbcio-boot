package com.nbcio.modules.estar.oa.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.nbcio.modules.estar.oa.entity.OaCalendar;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: OA日历表
 * @Author: nbacheng
 * @Date:   2023-05-04
 * @Version: V1.0
 */
public interface OaCalendarMapper extends BaseMapper<OaCalendar> {

}
