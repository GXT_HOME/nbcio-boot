/**/
package com.nbcio.modules.estar.bs.param;

import lombok.Data;
import java.io.Serializable;

import java.util.List;


/**
* @desc ReportDashboard 大屏设计查询输入类
* @author Raod
* @date 2021-04-12 14:52:21.761
**/
@Data
public class ReportDashboardParam extends PageParam implements Serializable{
}
