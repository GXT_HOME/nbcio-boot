package com.nbcio.modules.estar.tw.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.nbcio.modules.estar.tw.entity.TwInviteLink;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 项目邀请链接表
 * @Author: nbacheng
 * @Date:   2023-07-03
 * @Version: V1.0
 */
public interface TwInviteLinkMapper extends BaseMapper<TwInviteLink> {

}
