package com.nbcio.modules.estar.tw.service.impl;

import com.nbcio.modules.estar.tw.entity.TwLock;
import com.nbcio.modules.estar.tw.mapper.TwLockMapper;
import com.nbcio.modules.estar.tw.service.ITwLockService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 项目端节点表
 * @Author: nbacheng
 * @Date:   2023-07-03
 * @Version: V1.0
 */
@Service
public class TwLockServiceImpl extends ServiceImpl<TwLockMapper, TwLock> implements ITwLockService {

}
