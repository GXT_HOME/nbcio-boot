package com.nbcio.modules.estar.nd.service;

import com.nbcio.modules.estar.nd.entity.NdPicturefile;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: nd_picturefile
 * @Author: nbacheng
 * @Date:   2023-04-08
 * @Version: V1.0
 */
public interface INdPicturefileService extends IService<NdPicturefile> {

}
