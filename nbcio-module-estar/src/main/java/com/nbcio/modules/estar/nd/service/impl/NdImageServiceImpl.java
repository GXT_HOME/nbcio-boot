package com.nbcio.modules.estar.nd.service.impl;

import com.nbcio.modules.estar.nd.entity.NdImage;
import com.nbcio.modules.estar.nd.mapper.NdImageMapper;
import com.nbcio.modules.estar.nd.service.INdImageService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 网盘图像表
 * @Author: nbacheng
 * @Date:   2023-04-06
 * @Version: V1.0
 */
@Service
public class NdImageServiceImpl extends ServiceImpl<NdImageMapper, NdImage> implements INdImageService {

}
