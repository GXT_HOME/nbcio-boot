package com.nbcio.modules.estar.bs.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.nbcio.modules.estar.bs.dto.DataSetTransformDto;
import com.nbcio.modules.estar.bs.entity.DataSetTransform;

/**
* DataSetTransform Mapper
* @author nbacheng
* @date 2023-03-21
**/

public interface DataSetTransformMapper extends BaseMapper<DataSetTransform> {

}
