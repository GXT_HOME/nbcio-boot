package com.nbcio.modules.estar.nd.file;

public abstract class Reader {
    public abstract String read(ReadFile readFile);
}
