package com.nbcio.modules.estar.bs.mapper;


import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.nbcio.modules.estar.bs.param.DataSetParam;

/**
* DataSetParam Mapper
* @author nbacheng
* @date 2023-03-21
**/

public interface DataSetParamMapper extends BaseMapper<DataSetParam> {

}
