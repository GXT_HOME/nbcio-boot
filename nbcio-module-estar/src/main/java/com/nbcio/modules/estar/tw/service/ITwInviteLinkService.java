package com.nbcio.modules.estar.tw.service;

import com.nbcio.modules.estar.tw.entity.TwInviteLink;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 项目邀请链接表
 * @Author: nbacheng
 * @Date:   2023-07-03
 * @Version: V1.0
 */
public interface ITwInviteLinkService extends IService<TwInviteLink> {

}
