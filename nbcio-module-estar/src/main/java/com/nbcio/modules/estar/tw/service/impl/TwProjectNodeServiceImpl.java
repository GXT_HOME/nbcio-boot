package com.nbcio.modules.estar.tw.service.impl;

import com.nbcio.modules.estar.tw.entity.TwProjectNode;
import com.nbcio.modules.estar.tw.mapper.TwProjectNodeMapper;
import com.nbcio.modules.estar.tw.service.ITwProjectNodeService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 项目端节点表
 * @Author: nbacheng
 * @Date:   2023-07-03
 * @Version: V1.0
 */
@Service
public class TwProjectNodeServiceImpl extends ServiceImpl<TwProjectNodeMapper, TwProjectNode> implements ITwProjectNodeService {

}
