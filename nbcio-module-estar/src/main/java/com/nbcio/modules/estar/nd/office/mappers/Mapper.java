/**
 *
 * (c) Copyright Ascensio System SIA 2021
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.nbcio.modules.estar.nd.office.mappers;

import com.nbcio.modules.estar.nd.office.documentserver.models.AbstractModel;
import com.nbcio.modules.estar.nd.office.entities.AbstractEntity;
import com.nbcio.modules.flowable.apithird.entity.SysUser;

// specify the model mapper functions
public interface Mapper<E extends AbstractEntity, M extends AbstractModel> {
	M toModel(E entity);  // convert the entity to the model
}
