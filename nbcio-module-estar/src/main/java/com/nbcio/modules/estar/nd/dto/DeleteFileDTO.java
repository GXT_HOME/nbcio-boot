package com.nbcio.modules.estar.nd.dto;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class DeleteFileDTO {
	@ApiModelProperty(value="用户文件id", required=true)
    private String userFileId;


}