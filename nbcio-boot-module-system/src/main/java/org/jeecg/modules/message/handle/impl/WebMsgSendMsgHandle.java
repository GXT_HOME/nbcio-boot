package org.jeecg.modules.message.handle.impl;

import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.List;

import org.jeecg.common.api.dto.message.MessageDTO;
import org.jeecg.common.system.api.ISysBaseAPI;
import org.jeecg.common.util.SpringContextUtils;
import org.jeecg.modules.message.handle.ISendMsgHandle;


@Slf4j
public class WebMsgSendMsgHandle implements ISendMsgHandle {

	@Override
	public void SendMsg(String es_receiver, String es_title, String es_content, String es_fromuser) {
		log.info("WebMsgSendMsgHandle 发websocket消息");
		ISysBaseAPI sysBaseAPI = (ISysBaseAPI) SpringContextUtils.getBean(ISysBaseAPI.class);
		List<String> userlist = Arrays.asList(es_receiver.split(","));
		for(String user : userlist) {
			MessageDTO messagedto = new MessageDTO();
			messagedto.setContent(es_content);
			messagedto.setCategory("1"); //放到通知通告了
			messagedto.setFromUser(es_fromuser);
			messagedto.setTitle(es_title);
			messagedto.setToUser(user);
			sysBaseAPI.sendSysAnnouncement(messagedto);
		}
		
	}

}
